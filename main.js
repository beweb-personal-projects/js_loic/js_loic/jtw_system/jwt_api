const express = require('express')
const app = express()
const port = 1780
const bodyParser = require('body-parser')
const axios = require('axios').default;
const cors = require('cors');

/* ------------------------------- MIDDLEWARES ------------------------------ */
app.use(cors());
app.use(bodyParser.json());

app.use((req, res, next) => {
  if(req.path === '/authorize') {
    console.log(true);
    next();
  } else {
    axios.post('http://172.17.0.1:1770/auth/verify', req.headers)
    .then(response => next())
    .catch(error => res.send('UnAuthorized MW').status(403))
  }
})

/* --------------------------------- ROUTING -------------------------------- */
app.get('/', (req, res) => {
  res.send('Hello World API-SERVER!')
})

app.listen(port, () => {
  console.log(`[API-SERVER] Example app listening on port ${port}`)
})

/* -------------------------------------------------------------------------- */

app.post('/authorize', (req, res) => {
  axios.post('http://172.17.0.1:1770/authorize', {
    name: req.body.name,
    password: req.body.password
  })
  .then(response => {
    res.send(response.data).status(200);
  })
  .catch(error => {
    res.send(error).status(403);
  })
})